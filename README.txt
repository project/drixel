Original Pixel (formerly known as Pixeled) Wordpress design and theme by http://samk.ca

The Drupal version of Pixel (Drixel) is built on the Framework theme. The Framework theme is by Andre Griffen.

Ported into Drupal with Drupal specific styling by SteveJBayer.

The following supported modules/features have code contributed by:
Image_FUpload (Mark_Watson27)
RTL Support (tsi)

---Note for users upgrading from Alpha 14 or earlier:---

-Drixel was formerly known as the Pixeled theme and has been renamed to Drixel to avoid infringing a pre-registered trademark that was not known at the time of the theme conversion to Drupal.

-While the project name is Drixel, the current theme is uses the name Pixeled.

-If you have used an Alpha 14 (or earlier( and have now installed an Alpha15 (or later,) you may have 2 folders: 'Pixeled' and 'drixel.' For those with 2 folders with the names 'Pixeled' and 'drixel,' please delete the Pixeled folder. 

--------------------------------------------------------

For those interested in having the HTML 5 version of Drixel, please check out:
http://drupal.org/project/drixel5